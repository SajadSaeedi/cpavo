#include <math.h>
#include <scamp5.hpp>
#include <iostream>
#include <vs_scamp5.hpp>
#include <scamp5_output.hpp>
#include <Eigen/Dense>
#include <random>

using namespace SCAMP5_PE;

vs_dotmat dotmat;

#define GLOBAL_SUM_SAMPLES 64
uint8_t sum_results[GLOBAL_SUM_SAMPLES];
enum class SHIFT_DIRECTIONS { NONE, NORTH, SOUTH, EAST, WEST };

const int max_horizontal_blocks = 8;
const int max_verticle_blocks = 8;

int block_x_shifts[max_horizontal_blocks*max_verticle_blocks];
int block_y_shifts[max_horizontal_blocks*max_verticle_blocks];

volatile int readout_mode;
volatile int threshold;


int evaluate_shift_direction()
{
	scamp5_kernel_begin();
	//create absolute difference image between current shifted image and previous frame
	sub(F, E, B);
	abs(E, F);

	//apply block mask stored in R5
	WHERE(R5);
	mov(E, D);
	all();
	scamp5_kernel_end();

	//perform global sum
#if GLOBAL_SUM_SAMPLES == 16
	scamp5_global_sum_4x4(E, sum_results);
#endif
#if GLOBAL_SUM_SAMPLES == 64
	scamp5_global_sum_8x8(E, sum_results);
#endif

	//calculate global sum total from samples
	int total_sum_value = 0;
	for (int n = 0; n < GLOBAL_SUM_SAMPLES; n++)
	{
		total_sum_value = total_sum_value + sum_results[n];
	}

	return total_sum_value;
}


std::vector<unsigned int> get_random(unsigned int min, unsigned int max, unsigned int howmany)
{
	std::vector<unsigned int> random_numbers;

	std::vector<unsigned int> a;
	std::vector<unsigned int>::iterator it;

	// Our Random Generator
	std::mt19937 eng{ std::random_device{}() };

	for (int iPoint = 1; iPoint <= howmany; iPoint++)
	{
		unsigned int q = std::uniform_int_distribution<unsigned int>{ min, max }(eng);

		do {
			q = std::uniform_int_distribution<unsigned int>{ min, max }(eng);
			it = std::find(a.begin(), a.end(), q);
		} while (it != a.end());

		a.push_back(q);

		//std::cout << q << std::endl;
		random_numbers.push_back(q);
	}

	return random_numbers;
}


Eigen::MatrixXf  generate_base_vectors(int horizontal_blocks, int verticle_blocks)
{
	// implementaion needs to inlcude all block sizes. It only supports 4x4 blocks

	/*
	Eigen::MatrixXf matrixB(4, 2 * horizontal_blocks*verticle_blocks);

	
	matrixB << 3, -3, 3, -1, 3, 1, 3, 3, 1, -3, 1, -1, 1, 1, 1, 3, -1, -3, -1, -1, -1, 1, -1, 3, -3, -3, -3, -1, -3, 1, -3, 3,
	-3, -3, -1, -3, 1, -3, 3, -3, -3, -1, -1, -1, 1, -1, 3, -1, -3, 1, -1, 1, 1, 1, 3, 1, -3, 3, -1, 3, 1, 3, 3, 3,
	1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1;
	
	// roll
	// z (zoom)
	// yaw
	// pitch
	
	//matrixB << -3, 3, -3, 1, -3, -1, -3, -3, -1, 3, -1, 1, -1, -1, -1, -3, 1, 3, 1, 1, 1, -1, 1, -3, 3, 3, 3, 1, 3, -1, 3, -3,
	//	       -3, -3, -1, -3, 1, -3, 3, -3, -3, -1, -1, -1, 1, -1, 3, -1, -3, 1, -1, 1, 1, 1, 3, 1, -3, 3, -1, 3, 1, 3, 3, 3,
	//	        1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0,
	//	        0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1;
	//			

	std::cout << matrixB << std::endl;

	matrixB.row(0) = matrixB.row(0) / matrixB.row(0).norm();
	matrixB.row(1) = matrixB.row(1) / matrixB.row(1).norm();
	matrixB.row(2) = matrixB.row(2) / matrixB.row(2).norm();
	matrixB.row(3) = matrixB.row(3) / matrixB.row(3).norm();

	Eigen::MatrixXf H = matrixB.transpose();
	*/
	//std::cout << H.transpose() * H << std::endl;
	// The solution is (H'H)^(-1) * H' * m, but (H'H)^(-1) = I, i.e. H is orthonoraml

	int computation_block_x = horizontal_blocks * 2;
	int computation_block_y = verticle_blocks * 2;

	int c_x = horizontal_blocks;
	int c_y = verticle_blocks;

	Eigen::VectorXf     z( 2 * horizontal_blocks*verticle_blocks, 1);
	Eigen::VectorXf  roll( 2 * horizontal_blocks*verticle_blocks, 1);
	Eigen::VectorXf pitch( 2 * horizontal_blocks*verticle_blocks, 1);
	Eigen::VectorXf   yaw( 2 * horizontal_blocks*verticle_blocks, 1);

	int i = 0;
	for (int idx = 0; idx < horizontal_blocks; idx++)
	{
		for (int idy = 0; idy < verticle_blocks; idy++)
		{
			int block_index = idy * horizontal_blocks + idx;
			int x = 2 * idx - (c_x - 1);
			int y = 2 * idy - (c_y - 1);

			i = 2 * block_index;
			z(i) = x;
			roll(i) = -y;
			pitch(i) = 0;
			yaw(i) = 1;
			
			i = i + 1;
			z(i) = y;
			roll(i) = x;
			pitch(i) = 1;
			yaw(i) = 0;
		}
	}

	Eigen::MatrixXf H1(2 * horizontal_blocks*verticle_blocks, 4);

	roll = roll / roll.norm();
	z = z / z.norm();
	yaw = yaw / yaw.norm();
	pitch = pitch / pitch.norm();

	H1.col(0) = roll;
	H1.col(1) = z;
	H1.col(2) = yaw;
	H1.col(3) = pitch;

	return H1;
}



Eigen::Vector4f estimate_ransac_motion(Eigen::MatrixXf H, Eigen::VectorXf measurement, int horizontal_blocks, int verticle_blocks, float inlier_threshold)
{
	//-------- RANSAC parameters
	//-------- these parameters need to be adjusted 
	int nPoints = 5;             // number of points in each sample
	float probability = 0.99;    // probability
	unsigned int N = horizontal_blocks * verticle_blocks;
	int minInlier = 4;			 // these need to be adjusted
	int maxInteration = 200;
	float eps = (float)(minInlier) / (float)(N);
	int iItr = 0;

	if (minInlier == N)
		iItr = 1;
	else
		iItr = ceil(log(1 - probability) / log(1 - pow(eps, nPoints)));
	
	maxInteration = std::max(1, std::min(iItr, maxInteration));

	int iBestInliers = 0;
	//std::cout << "maxInteration: " << maxInteration << std::endl;
	
	//-------- RANSAC vectors and containters
	Eigen::Vector4f best_motion_ransac;
	best_motion_ransac << 0.0, 0.0, 0.0, 0.0;

	Eigen::Vector4f motion_ransac;
	motion_ransac << 0.0, 0.0, 0.0, 0.0;

	Eigen::VectorXf error_ransac;
	error_ransac = Eigen::VectorXf::Zero(2 * horizontal_blocks*verticle_blocks, 1);

	//-------- looping for RANSAC
	for (int iRansacIteration = 0; iRansacIteration < maxInteration; iRansacIteration++)
	{
		// generate nPoints random number between [0 N-1]
		std::vector<unsigned int> random_numbers = get_random(0, N - 1, nPoints);
		//motion_ransac = calulate_ransac_motion(H, random_numbers);

		// get the random points from the model
		Eigen::MatrixXf subH = Eigen::MatrixXf::Zero(2 * nPoints, 4);
		Eigen::VectorXf subM = Eigen::VectorXf::Zero(2 * nPoints, 1);

		int iSubHindex = 0;
		for (std::vector<unsigned int>::iterator it = random_numbers.begin(); it != random_numbers.end(); ++it)
		{
			//std::cout << *(it) << std::endl;
			unsigned int i = *(it);
			int block_index = i;
			subH.row(iSubHindex) = H.row(2 * i);
			subM(iSubHindex) = measurement(2 * i);
			//subM(iSubHindex) = block_x_shifts[i];

			iSubHindex = iSubHindex + 1;
			subH.row(iSubHindex) = H.row(2 * i + 1);
			subM(iSubHindex)     = measurement(2 * i +1 );
			//subM(iSubHindex) = block_y_shifts[i];

			iSubHindex = iSubHindex + 1;
		}

		//solve for the motion (H'H)^(-1) * H' * m
		motion_ransac = ((subH.transpose() * subH).inverse()) * subH.transpose() * subM;

		/*std::cout << " alfa-ransac: " << motion_ransac(0)
		<< " beta-ransac: " << motion_ransac(1)
		<< " gama-ransac: " << motion_ransac(2)
		<< " delta-ransac: " << motion_ransac(3) << std::endl;*/


		// estimates_ransac = H * motion_ransac;
		error_ransac = measurement - H * motion_ransac;

		std::vector<float> vError;
		std::vector<bool>  vInliers;


		int iNumberofInliers = 0;

		for (int i = 0; i < N; i++)
		{
			float err = sqrt(error_ransac(2 * i)*error_ransac(2 * i) + error_ransac(2 * i + 1)*error_ransac(2 * i + 1));
			vError.push_back(err);
			//std::cout << err << std::endl;

			if (err < inlier_threshold)
			{
				vInliers.push_back(true);
				iNumberofInliers = iNumberofInliers + 1;
			}
			else
				vInliers.push_back(false);

		}

		//std::cout << "# inliers: " << iNumberofInliers << std::endl;

		if (iNumberofInliers > iBestInliers)
		{
			iBestInliers = iNumberofInliers;
			best_motion_ransac = motion_ransac;
		}
	}

	return best_motion_ransac;
}

Eigen::Vector4f estimate_OLS_motion(Eigen::MatrixXf H, Eigen::VectorXf measurement)
{
	Eigen::Vector4f motion;
	motion << 0.0, 0.0, 0.0, 0.0;
	
	motion = H.transpose() * measurement;
	/*std::cout << " alfa: " << motion(0)
	<< " beta: " << motion(1)
	<< " gama: " << motion(2)
	<< " delta: " << motion(3) << std::endl;*/

	return motion;
}

void scamp5_main() {

	int horizontal_blocks = 4, verticle_blocks = 4;
	int blocks = 4;
	int search_iterations = 5;
	int iInlierThreshold = 5;

	auto display_1 = vs_gui_add_display("measurements", 0, 0, 1);
	auto display_2 = vs_gui_add_display("image", 1, 0, 1);
	auto display_3 = vs_gui_add_display("estimates_OLS", 0, 1, 1);
	auto display_4 = vs_gui_add_display("estimates_RANSAC", 1, 1, 1);

	vs_gui_add_slider("blocks (N X N): ", 2, max_verticle_blocks, 4, &blocks);
	//vs_gui_add_slider("horizontal_blocks: ", 1, max_horizontal_blocks, 4, &horizontal_blocks);
	//vs_gui_add_slider("verticle_blocks: ", 1, max_verticle_blocks, 4, &verticle_blocks);
	vs_gui_add_slider("search_iterations: ", 1, 40, 5, &search_iterations);
	vs_gui_add_slider("inlier threshold(/10): ", 1, 10, 5, &iInlierThreshold);

	

	vs_on_gui_update(VS_GUI_FRAME_RATE, [&](int32_t new_value) {
		uint32_t framerate = new_value;
		if (framerate > 0) {
			vs_frame_trigger_set(1, framerate);
			vs_enable_frame_trigger();
			vs_post_text("frame trigger: 1/%d\n", (int)framerate);
		}
		else {
			vs_disable_frame_trigger();
			vs_post_text("frame trigger disabled\n");
		}
	});

	vs_on_host_connect([&]() {
		vs_post_text("Scamp5d Image Storage\n");
		vs_post_text("loop_counter: %d\n", (int)vs_loop_counter_get());
		scamp5_kernel::print_debug_info();
		vs_led_on(VS_LED_2);
	});

	vs_on_host_disconnect([&]() {
		vs_led_off(VS_LED_2);
	});

	vs_frame_trigger_set(1, 50);
	
	//main loop
	while (1) {
		if (blocks % 2 == 0)
		{
			horizontal_blocks = blocks;
			verticle_blocks = blocks;
		}
		else
		{
			horizontal_blocks = 4;
			verticle_blocks = 4;
			std::cerr << "Odd size blocks is not supported. Default is set to 4x4" << std::endl;
		}

		// this function is where all the interaction with host happens
		vs_process_message();

		// frame trigger
		vs_wait_frame_trigger();

		//bind post channel to display 1 for showing dot mat
		vs_post_set_channel((uint32_t)display_1);

		//store previous image
		scamp5_kernel_begin();
		mov(B, A);
		scamp5_kernel_end();

		//get new image
		scamp5_get_image(A, E, vs_gui_read_slider(VS_GUI_FRAME_GAIN));

		int block_size_x = round(256.0 / horizontal_blocks);
		int block_size_y = round(256.0 / verticle_blocks);
		for (int x = 0; x < horizontal_blocks; x++)
		{
			for (int y = 0; y < verticle_blocks; y++)
			{
				int block_index = y * horizontal_blocks + x;

				int min_sum_value = 0;

				int WEST_shift_score = 0;
				int EAST_shift_score = 0;
				int SOUTH_shift_score = 0;
				int NORTH_shift_score = 0;

				//create mask for current block
				scamp5_load_rect(R1, y*block_size_y, x*block_size_x, (y + 1)*block_size_y - 1, (x + 1)*block_size_x - 1);
				//scamp5_load_rect(R1, 3*64, 0, 4*64-1, 64);
				scamp5_kernel_begin();
				NOT(R5, R1);

				//copy current image
				mov(C, A);
				scamp5_kernel_end();

				//fill D with mask value
				scamp5_in(D, 0);

				//////////////////////////////////////////////////////////////////////////////////////
				//EVALUTE NO SHIFT
				scamp5_kernel_begin();
				mov(E, C);
				scamp5_kernel_end();
				min_sum_value = evaluate_shift_direction();
				SHIFT_DIRECTIONS chosen_action = SHIFT_DIRECTIONS::NONE;

				for (int n = 0; n < search_iterations; n++)
				{
					//reset chosen action
					chosen_action = SHIFT_DIRECTIONS::NONE;

					//////////////////////////////////////////////////////////////////////////////////////
					//EVALUTE EAST SHIFT
					scamp5_kernel_begin();
					mov(E, C, east);
					scamp5_kernel_end();

					EAST_shift_score = evaluate_shift_direction();
					if (min_sum_value > EAST_shift_score)
					{
						min_sum_value = EAST_shift_score;
						chosen_action = SHIFT_DIRECTIONS::EAST;
					}

					//////////////////////////////////////////////////////////////////////////////////////
					//EVALUTE EAST SHIFT
					scamp5_kernel_begin();
					mov(E, C, west);
					scamp5_kernel_end();

					WEST_shift_score = evaluate_shift_direction();
					if (min_sum_value > WEST_shift_score)
					{
						min_sum_value = WEST_shift_score;
						chosen_action = SHIFT_DIRECTIONS::WEST;
					}

					//////////////////////////////////////////////////////////////////////////////////////
					//EVALUTE NORTH SHIFT
					scamp5_kernel_begin();
					mov(E, C, north);
					scamp5_kernel_end();

					NORTH_shift_score = evaluate_shift_direction();
					if (min_sum_value > NORTH_shift_score)
					{
						min_sum_value = NORTH_shift_score;
						chosen_action = SHIFT_DIRECTIONS::NORTH;
					}

					//////////////////////////////////////////////////////////////////////////////////////
					//EVALUTE SOUTH SHIFT
					scamp5_kernel_begin();
					mov(E, C, south);
					scamp5_kernel_end();

					SOUTH_shift_score = evaluate_shift_direction();
					if (min_sum_value > SOUTH_shift_score)
					{
						min_sum_value = SOUTH_shift_score;
						chosen_action = SHIFT_DIRECTIONS::SOUTH;
					}

					//////////////////////////////////////////////////////////////////////////////////////
					//SELECT BEST SHIFT DIRECTION
					switch (chosen_action)
					{
					case SHIFT_DIRECTIONS::EAST:
						scamp5_kernel_begin();
						mov(C, C, east);
						scamp5_kernel_end();
						block_x_shifts[block_index] = block_x_shifts[block_index] - 1;
						break;

					case SHIFT_DIRECTIONS::WEST:
						scamp5_kernel_begin();
						mov(C, C, west);
						scamp5_kernel_end();
						block_x_shifts[block_index] = block_x_shifts[block_index] + 1;
						break;

					case SHIFT_DIRECTIONS::NORTH:
						scamp5_kernel_begin();
						mov(C, C, north);
						scamp5_kernel_end();
						block_y_shifts[block_index] = block_y_shifts[block_index] - 1;
						break;

					case SHIFT_DIRECTIONS::SOUTH:
						scamp5_kernel_begin();
						mov(C, C, south);
						scamp5_kernel_end();
						block_y_shifts[block_index] = block_y_shifts[block_index] + 1;
						break;
					}

					//EXIT IF NO BEST DIRECTION
					if (chosen_action == SHIFT_DIRECTIONS::NONE)
					{
						break;
					}
				}
			}
		}

		Eigen::VectorXf measurement;
		Eigen::VectorXf estimates_OLS;
		Eigen::VectorXf estimates_ransac;

		measurement = Eigen::VectorXf::Zero(2 * horizontal_blocks*verticle_blocks, 1);
		estimates_OLS = Eigen::VectorXf::Zero(2 * horizontal_blocks*verticle_blocks, 1);
		estimates_ransac = Eigen::VectorXf::Zero(2 * horizontal_blocks*verticle_blocks, 1);

		//DRAW DIRECTION VECTORS FOR EACH BLOCK
		//dotmat.clear();
		//uint8_t * ptr = (uint8_t *)std::malloc(1000 * sizeof(uint8_t));
		//dotmat.bind_buffer(ptr, block_size_x, block_size_y);
		for (int x = 0; x < horizontal_blocks; x++)
		{
			for (int y = 0; y < verticle_blocks; y++)
			{
				int block_index = y * horizontal_blocks + x;
				int posx1 = block_size_x * (x + 0.5);
				int posy1 = block_size_y * (y + 0.5);
				int posx2 = posx1 + block_x_shifts[block_index] * 5;
				int posy2 = posy1 + block_y_shifts[block_index] * 5;
				//dotmat.draw_line(posy1, posx1, posy2, posx2, true);
				scamp5_draw_begin(R7);
				//scamp5_draw_line(posy1, posx1, posy2, posx2, false);
				scamp5_draw_line(posx1, posy1, posx2, posy2, false);
				scamp5_draw_end();

				int id = block_index * 2;
				measurement(id) = block_x_shifts[block_index];
				measurement(id + 1) = block_y_shifts[block_index];
				//std::cout << block_index << " ... (" << posx1  << ") , (" << posy1 << ") ... "<< block_x_shifts[block_index] << " ... " << block_y_shifts[block_index] << std::endl;

				//block_x_shifts[block_index] = 0;
				//block_y_shifts[block_index] = 0;
			}
		}
		//vs_post_dotmat(dotmat);



			// ---------------------  get model
			Eigen::MatrixXf H = generate_base_vectors(horizontal_blocks, verticle_blocks);


			// --------------------- get OLS estimates
			Eigen::Vector4f motion_OLS = estimate_OLS_motion(H, measurement);
			estimates_OLS = H * motion_OLS;

			// --------------------- plot OLS estimates
			for (int x = 0; x < horizontal_blocks; x++)
			{
				for (int y = 0; y < verticle_blocks; y++)
				{
					int block_index = y * horizontal_blocks + x;
					int posx1 = block_size_x * (x + 0.5);
					int posy1 = block_size_y * (y + 0.5);

					int id = block_index * 2;
					int posx2 = posx1 + estimates_OLS(id) * 5;
					int posy2 = posy1 + estimates_OLS(id + 1) * 5;
					scamp5_draw_begin(R8);
					//scamp5_draw_line(posy1, posx1, posy2, posx2, false);
					scamp5_draw_line(posx1, posy1, posx2, posy2, false);
					scamp5_draw_end();
				}
			}


			// --------------------- get RANSAC estimates
			float inlier_threshold = (float)iInlierThreshold / 10.0;
			//std::cout << "inlier_threshold: " << inlier_threshold << std::endl;
			Eigen::Vector4f motion_ransac = estimate_ransac_motion(H, measurement, horizontal_blocks, verticle_blocks, inlier_threshold);
			estimates_ransac = H * motion_ransac;

			// --------------------- plot RANSAC estimates
			for (int x = 0; x < horizontal_blocks; x++)
			{
				for (int y = 0; y < verticle_blocks; y++)
				{
					int block_index = y * horizontal_blocks + x;
					int posx1 = block_size_x * (x + 0.5);
					int posy1 = block_size_y * (y + 0.5);

					int id = block_index * 2;
					int posx2 = posx1 + estimates_ransac(id) * 5;
					int posy2 = posy1 + estimates_ransac(id + 1) * 5;
					scamp5_draw_begin(R9);
					//scamp5_draw_line(posy1, posx1, posy2, posx2, false);
					scamp5_draw_line(posx1, posy1, posx2, posy2, false);
					scamp5_draw_end();
				}
			}

		scamp5_output_image(R7, display_1);
		scamp5_output_image(R8, display_3);
		scamp5_output_image(R9, display_4);

		scamp5_output_image(C,display_2);

		scamp5_kernel_begin();
		    CLR(R7);
			CLR(R8);
			CLR(R9);
		scamp5_kernel_end();


		for (int x = 0; x < horizontal_blocks; x++)
		{
			for (int y = 0; y < verticle_blocks; y++)
			{
				int block_index = y * horizontal_blocks + x;
				block_x_shifts[block_index] = 0;
				block_y_shifts[block_index] = 0;
			}
		}

		//std::free(ptr);
		if (vs_loop_counter_get() % 25 == 0) {
			vs_led_toggle(VS_LED_1);
		}
		vs_loop_counter_inc();
	}

}

int main(){

    scamp5_sim::config("server_ip","127.0.0.1");
    scamp5_sim::config("server_port","27715");
    scamp5_sim::enable_keyboard_control();

    // initialise M0 system
    vs_init();

    // make default output to be USB
    vs_post_bind_io_agent(vs_usb);

    scamp5_bind_io_agent(vs_usb);

    vs_on_shutdown([&](){
        vs_post_text("M0 shutdown\n");
    });

    // run the vision algorithm

    scamp5_main();

    return 0;
}

